import {userRole} from "../utilities/enums";

export const isReaderValidation = (req, res, next) => {
    const inputUserRole = req.user.userRole;
    if (inputUserRole == userRole.reader) {
        return res.status(403).json();
    }
    next();
};

export const entityInDB = async (body, entity) => {
    let isInDB = await entity.where(body).fetch();
    if(!isInDB) {
        throw 404;
    }
    return isInDB;
};
