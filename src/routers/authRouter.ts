import * as express from 'express';
import {auth} from '../controllers/authController';
import {verifyUser} from "../middlewares/jwtHelper";

const router = express.Router();

router.post('/auth', auth);
router.get('/verify', verifyUser)

export default router;