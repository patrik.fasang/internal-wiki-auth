DROP SCHEMA IF EXISTS wiki CASCADE;
DROP DATABASE IF EXISTS "wiki_dev";
CREATE DATABASE "wiki_dev"
    WITH
    OWNER = "postgres"
    TEMPLATE = template0
    ENCODING = 'UTF8'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

\connect "wiki_dev";

CREATE SCHEMA "wiki"
    AUTHORIZATION "postgres";

--psql -U postgres -h localhost -a -f src/database/sql/devDat.sql